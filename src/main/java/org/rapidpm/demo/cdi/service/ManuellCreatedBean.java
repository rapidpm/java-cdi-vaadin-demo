package org.rapidpm.demo.cdi.service;


import javax.inject.Inject;

/**
 * Created with IntelliJ IDEA.
 * User: svenruppert
 * Date: 20.04.13
 * Time: 00:52
 * To change this template use File | Settings | File Templates.
 */


/**
 * Hierbei handelt es sich um ein Pojo mit injezierten Komponenten.
 *
 * Hier wird gezeigt wie eine Struktur aus Pojos und CDI-Beans aufgebaut werden kann.
 *
 */
public class ManuellCreatedBean {

    @Inject private GreetingService bean;

    public GreetingService getBean() {
        return bean;
    }
}
