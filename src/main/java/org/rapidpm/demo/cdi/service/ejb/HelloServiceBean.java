package org.rapidpm.demo.cdi.service.ejb;

import javax.ejb.Stateless;

/**
 * Created with IntelliJ IDEA.
 * User: svenruppert
 * Date: 19.04.13
 * Time: 01:18
 * To change this template use File | Settings | File Templates.
 */
@Stateless(name = "HelloServiceEJB")
public class HelloServiceBean {
    public HelloServiceBean() {
    }

    public String sayHello(){
        return "Say Hello EJB " + System.nanoTime();
    }


}
