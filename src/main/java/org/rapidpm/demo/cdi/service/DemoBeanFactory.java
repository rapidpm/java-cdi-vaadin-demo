package org.rapidpm.demo.cdi.service;

import javax.enterprise.inject.Produces;

/**
 * Created with IntelliJ IDEA.
 * User: svenruppert
 * Date: 20.04.13
 * Time: 01:59
 * To change this template use File | Settings | File Templates.
 */
public class DemoBeanFactory {

    /**
     * manuelles erzeugen von Instanzen.
     * @return
     */
    @Produces  InnerDemoClass createInnerDemoClass(){
        final InnerDemoClass i = new InnerDemoClass("...");
        i.setTime("Produced at " + System.nanoTime()+"");
        return i;
    }


    /**
     * Auch mit inneren statischen Klassen funktioniert CDI.
     *
     */
    public static class InnerDemoClass {

        public InnerDemoClass(String time) {
            this.time = time;
        }

        private String time;

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }
    }



}
