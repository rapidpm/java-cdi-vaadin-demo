package org.rapidpm.demo.cdi.service;


import com.vaadin.cdi.UIScoped;
import org.rapidpm.demo.cdi.service.DemoBeanFactory;
import org.rapidpm.demo.cdi.service.ManuallCreatedTimer;
import org.rapidpm.demo.cdi.service.ManuellCreatedBean;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;

/**
 * Created with IntelliJ IDEA.
 * User: svenruppert
 * Date: 20.04.13
 * Time: 01:17
 * To change this template use File | Settings | File Templates.
 */

@UIScoped
public  class BeanCreator {


    @Inject
    Instance<ManuellCreatedBean> manuellCreatedBean;

    @Inject
    Instance<ManuallCreatedTimer> manuellCreatedTimers;


    @Inject Instance<DemoBeanFactory.InnerDemoClass> innerDemoClasses;

    public Instance<ManuellCreatedBean> getManuellCreatedBean() {
        return manuellCreatedBean;
    }

    public Instance<ManuallCreatedTimer> getManuellCreatedTimers() {
        return manuellCreatedTimers;
    }

    public Instance<DemoBeanFactory.InnerDemoClass> getInnerDemoClasses() {
        return innerDemoClasses;
    }
}
