package org.rapidpm.demo.cdi.service;

import com.vaadin.cdi.UIScoped;


/**
 * Created with IntelliJ IDEA.
 * User: svenruppert
 * Date: 18.04.13
 * Time: 23:54
 * To change this template use File | Settings | File Templates.
 */



@UIScoped
public class GreetingService {

    public String sayHello(String name) {
        return "Hello " + name + "! from GreetingService";
    }
}
