package org.rapidpm.demo.cdi.view;

import com.vaadin.cdi.CDIViewProvider;
import com.vaadin.server.VaadinRequest;
import com.vaadin.cdi.CDIUI;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import org.rapidpm.demo.cdi.service.BeanCreator;
import org.rapidpm.demo.cdi.service.GreetingService;
import org.rapidpm.demo.cdi.service.ManuellCreatedBean;
import org.rapidpm.demo.cdi.service.ejb.HelloServiceBean;

import javax.ejb.EJB;
import javax.inject.Inject;

/**
 * The Application's "main" class
 */
@SuppressWarnings("serial")
@CDIUI
public class MyVaadinUI extends UI {


    @Inject
    private GreetingService greetingService;

    @Inject
    private CDIViewProvider viewProvider;

    @EJB(beanName = "HelloServiceEJB")
    private HelloServiceBean helloServiceBean;


    @Override
    protected void init(VaadinRequest request) {
        final VerticalLayout layout = new VerticalLayout();
        layout.setMargin(true);
        final Label header = new Label("My Vaadin Application");
        header.setStyleName("h1");
        layout.addComponent(header);

        final TextField nameField = new TextField("What is your name?");
        final Label greetingLbl = new Label();
        layout.addComponent(nameField);
        layout.addComponent(greetingLbl);

        layout.addComponent(new Button("Say Hello", new Button.ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
                final String name = nameField.getValue();
                String caption = greetingService.sayHello(name) +" -- ";
                caption = caption + helloServiceBean.sayHello() +" -- ";
                caption = caption + "cascade.." + beanCreator.getManuellCreatedBean().get().getBean().sayHello(name)+" -- ";
                caption = caption + beanCreator.getManuellCreatedTimers().get().getTime()+" -- ";
                caption = caption + beanCreator.getInnerDemoClasses().get().getTime()+" -- ";
                greetingLbl.setCaption(caption);
            }
        }));
        layout.addComponent(new Button("Close Application", new Button.ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                close();
            }
        }));
        setContent(layout);
    }


    @Inject
    private BeanCreator beanCreator;

    @Inject
    private ManuellCreatedBean manuellCreatedBean;

}
